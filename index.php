<?php
date_default_timezone_set('America/Sao_Paulo');
include('class/telegram_chat.class.php');                                       //Possui à estrutura de dados necessária para integração
include('sql/telegram_chat.sql.php');                                           //Possui às funções que são executadas no MySQL
include('inc/telegram_api.inc.php');                                            //Possui às API's do Telegram utilizada no projeto
include('dao/telegram_chat.php');                                               //Estrutura codificada para funcionamento do sistema
include('class/download.class.php');                                            //Inclui à classe ClassDownload
include('inc/download.inc.php');                                                //Inclui à função DownloadPhoto

$update = json_decode(file_get_contents("php://input"), true);

if (!$update) { exit; }

//Types Recv
define('Msg', 1);
define('Photo', 2);
if (isset($update['message']['caption'])) {
    recv($update['message'], Photo);
    exit;
}

if (isset($update['message']['text'])) {
    recv($update['message'], Msg);
    exit;
}
?>