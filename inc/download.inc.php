<?php
/*- Arquivos são encodados com sha1
  - 2 primeiros digitos do sha1 é utilizado como nome de subpasta
  - glpi_documents é utilizado para salvar o nome do arquivo feito o upload, diretório, entre outros..
  - glpi_documents_items utilizado para salvar o ID do glpi_documents, items_id (id do ticket), tipo de Item, entre outros..
  - http://php.net/manual/pt_BR/function.sha1-file.php - exemplo de encode file*/
  define('URL_Download' , 'https://api.telegram.org/file/bot'.BOT_TOKEN.'/');
  define('Folder_Temp'  , '../files/_tmp/');
  define('Folder_JPG'   , '../files/JPG/');

  function StreamFile_Download($url){                                           //Baixa os dados do arquivo em memória
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_AUTOREFERER, false);
      curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      $result = curl_exec($ch);
      curl_close($ch);
      return($result);
  }

  function CreateArchive($new_filename, $StreamFile){                           //Cria e grava os dados recebidos do Curl no arquivo
      $fp = fopen($new_filename, 'w');
      fwrite($fp, $StreamFile);
      fclose($fp);
  }

  function FileNameSHA1($str){                                                  //retorna a string FileName em SHA-1 sem os primeiros 2 caracteres
    for($i = 2; $i <= strlen($str); $i++)
    {
      $FileName .= $str[$i];

      if($i == strlen($str)) return $FileName;
    }
  }

  function DownloadPhoto($Telegram){
    $FileDir  = Folder_Temp.basename($Telegram->getFile_Path());                //Caminho onde o arquivo vai ser baixado.jpg
    $url      = URL_Download.$Telegram->getFile_Path();                         //URL para download da foto
    $StreamFile = StreamFile_Download($url);                                    //Carrega os dados do arquivo de download
    CreateArchive($FileDir, $StreamFile);                                       //Cria o arquivo no diretório e salva os dados

    $PhotoDownload  = new ClassDownload();

    $PhotoDownload->setHashFile(sha1_file($FileDir));                           //Calcula o Hash do arquivo
    $PhotoDownload->setDir(Folder_JPG.$PhotoDownload->getHashFile()[0]          //Diretório que o arquivo vai ser salvo, com as iniciais do Hash
                                     .$PhotoDownload->getHashFile()[1].chr(47));
    $PhotoDownload->setFileName(FileNameSHA1($PhotoDownload->getHashFile()).'.jpg');   //Nome do arquivo SHA1.jpg
    $PhotoDownload->setArqName(basename($FileDir));                             //nome do arquivo enviado.jpg

    mkdir($PhotoDownload->getDir(), 0777);                                      //cria o diretório com a permissão 777
    if(rename($FileDir, $PhotoDownload->getDir().$PhotoDownload->getFileName()))//utilizado para mover o arquivo para a pasta correta
      $PhotoDownload->setBool(true);
    return $PhotoDownload;
  }

?>
