<?php

//Functions Telegram
function apiRequest($method, $parameters) {
    if (!is_string($method)) {
        error_log("Method name must be a string\n");
        return false;
    }

    if (!$parameters) {
        $parameters = array();
    } else if (!is_array($parameters)) {
        error_log("Parameters must be an array\n");
        return false;
    }

    foreach ($parameters as $key => &$val) {
        // encoding to JSON array parameters, for example reply_markup
        if (!is_numeric($val) && !is_string($val)) {
            $val = json_encode($val);
        }
    }
    $url = API_URL . $method . '?' . http_build_query($parameters);
    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);

    return curl_exec($handle);
}

function sendMsg($ChatID, $Msg) {
    $result_encoded = apiRequest("sendMessage", array("parse_mode" => 'HTML', "chat_id" => $ChatID, "text" => $Msg));

    return json_decode($result_encoded, true);
}

function deleteMsg($ChatID, $message_id) {
    return apiRequest("deleteMessage", array("chat_id" => $ChatID, "message_id" => $message_id));
}

function isMemberGroup($user_id) {
    $result = apiRequest("getChatMember", array("chat_id" => Chat_ID, "user_id" => $user_id));
    
    $result = json_decode($result, true);
    
    if ($result['result']['status'] == 'member' || $result['result']['status'] == 'administrator')
        return true;
    
    return false;
}

?>