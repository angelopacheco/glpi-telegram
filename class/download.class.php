<?php
  class ClassDownload{
    private $HashFile   = "";
    private $Dir        = "";
    private $FileName   = "";
    private $ArqName    = "";
    private $bool       = false;

    //Set
    public function setHashFile($value){
      $this->HashFile = $value;
    }

    public function setDir($value){
      $this->Dir = $value;
    }

    public function setFileName($value){
      $this->FileName = $value;
    }

    public function setArqName($value){
      $this->ArqName = $value;
    }

    public function setBool($value){
      $this->bool = $value;
    }

    //Get
    public function getHashFile(){
      return $this->HashFile;
    }

    public function getDir(){
      return $this->Dir;
    }

    public function getFileName(){
      return $this->FileName;
    }

    public function getArqName(){
      return $this->ArqName;
    }

    public function getBool(){
      return $this->bool;
    }
  }
?>
