<?php

class TelegramChat {
    //Var
    private $Chat_ID    = 0;                                                    //ID do Chat / Grupo
    private $Message    = "";                                                   //Mensagem recebida
    private $date       = 0;                                                    //Data da mensagem em formato Unix
    private $status     = 0;                                                    //Status do chamado
    private $From_ID    = 0;                                                    //ID do Usuário que enviou à mensagem
    private $FirstName  = "";                                                   //Primeiro nome do usuário que enviou à mensagem
    private $LastName   = "";                                                   //Último nome do usuário que enviou à mensagem
    private $User       = "";                                                   //Nome de usuário do painel
    private $ID_Ticket  = 0;                                                    //ID do Ticket (Utilizado no Follow Up)
    private $File_ID    = "";                                                   //Utilizado no getFile para pegar o caminho do documento
    private $File_Path  = "";                                                   //Caminho do documento enviado (foto, pdf, ...)
    private $Filial_ID  = 0;                                                    //ID da Filial

    //Set
    public function setChat_ID($value) {
        $this->Chat_ID = $value;
    }

    public function setMessage($value) {
        $this->Message = $value;
    }
    
    public function setDate($value){
        $this->date = $value;
    }
    
    public function setStatus($value){
        $this->status = $value;
    }

    public function setFrom_ID($value) {
        $this->From_ID = $value;
    }

    public function setFirstName($value) {
        $this->FirstName = $value;
    }

    public function setLastName($value) {
        $this->LastName = $value;
    }

    public function setUser($value) {
        $this->User = $value;
    }

    public function setID_Ticket($value) {
        $this->ID_Ticket = $value;
    }

    public function setFile_ID($value) {
        $this->File_ID = $value;
    }

    public function setFile_Path($value) {
        $this->File_Path = $value;
    }

    //Get
    public function getChat_ID() {
        return $this->Chat_ID;
    }

    public function getMessage() {
        return $this->Message;
    }
    
    public function getDate(){
        return $this->date;
    }

    public function getFrom_ID() {
        return $this->From_ID;
    }

    public function getFirstName() {
        return $this->FirstName;
    }

    public function getLastName() {
        return $this->LastName;
    }

    public function getUser() {
        return $this->User;
    }

    public function getID_Ticket() {
        return $this->ID_Ticket;
    }

    public function getFile_ID() {
        return $this->File_ID;
    }

    public function getFile_Path() {
        return $this->File_Path;
    }
    
    public function getID_Filial(){
        return $this->Filial_ID;
    }

    public function ExtractID_Filial() {
        $text = trim(preg_replace('/( )+/', chr(32), $this->getMessage()));     //Retira os espaços do início e do final, e espaços duplos

        $result = explode(chr(32), $text);                                      //Separa à String por espaço

        $this->Filial_ID = intval($result[1]);                                  //Transforma o valor string da posição 1 em inteiro

        if (($this->Filial_ID <= 0) || (strlen($this->Filial_ID) > 4))          //Caso o valor inteiro seja <= 0 ou o id da filial seja maior que 4, retorna falso
            return false;

        return $this->Filial_ID;                                                //Retorna o id da filial
    }
    
    public function getStatus(){
        switch($this->status){
            case 1:
                return 'Novo';
                break;
            case 2:
                return 'Processando (atribuído)';
                break;
            case 3:
                return 'Processando (planejado)';
                break;
            case 4:
                return 'Pendente';
                break;
            case 5:
                return 'Solucionado';
                break;
            case 6:
                return 'Fechado';
                break;
            default:
                return 'Erro!';
                break;
        }
    }

    //Mensagens
    public function Msg_CreateTicket() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, sua solicitação foi encaminhada para análise, em breve retornaremos!';
    }

    public function Msg_DadosTicket() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>,

recebemos seu chamado, para adicionar mais informações ao mesmo ou responde-lo digite:
<b>@' . $this->getID_Ticket() . '</b> número_da_filial <i>texto</i>.

<b>Chamado referente ao:</b>

' . $this->getMessage().'

<b>Link do chamado:</b>
https://servicedesk.restaurantemadero.com.br/front/ticket.form.php?id='.$this->getID_Ticket();
    }
    
    public function Msg_DadosTicketPlantao($bool){
        return 'Novo chamado aberto por ( <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b> )

<b>Descrição:</b>
"'.$this->getMessage().'"

<b>Contém anexos:</b> '.($bool ? 'Sim':'Não').'

<b>Link do chamado:</b>
https://servicedesk.restaurantemadero.com.br/front/ticket.form.php?id='.$this->getID_Ticket();
    }

    public function Msg_FailCreateTicket() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, não foi possível criar seu ticket, tente novamente ou entre em contato com o suporte técnico!';
    }

    public function Msg_AttachAccount_(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, identificamos que seu número não está cadastrado em nosso suporte, por favor, <b>responda aqui mesmo digitando</b> apenas seu nome de usuário do Service Desk!

<b>Exemplo:</b>
@nome.sobrenome
(Necessário o arroba "@")';
    }
    
    public function Msg_AttachAccount(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, identificamos que seu número não está cadastrado em nosso suporte, por favor me responda apenas seu nome de usuário do Service Desk!

<b>Exemplo:</b>
@login
(Necessário o arroba "@")';
    }
    
    public function Msg_AttachedSuccess($Boot_Name){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, seu Telegram foi vinculado ao usuário ( @'.trim($this->getMessage()).' ) com sucesso! <b>Agora você terá que voltar ao grupo e abrir o chamado novamente, digitando: '.$Boot_Name.'  numero_de_filial  Texto.</b>';
    }
    
    public function Msg_AttachedError(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, não foi possível vincular seu Telegram ao usuário informado ( @'.trim($this->getMessage()).' ), certifique os dados e envie novamente o nome de usuário!';
    }

    public function Msg_ErrorMySQL() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, possivelmente estamos em manutenção ou com problemas em nosso banco de dados.. Tente novamente, ou entre em contato com o suporte técnico!';
    }

    public function Msg_Flood($name) {
        return 'Olá membros,

O grupo <b>Atendimento Madero</b> no Telegram foi criado só para abrir chamados em nosso suporte. Se você quiser adicionar informações à um chamado existente, terá que realiza-la no chat Suporte Madero (SM), com o @numero_do_chamado texto_da_mensagem.

Para abrir um novo chamado no suporte digite: <b>' . $name . '</b> número_da_filial <i>texto</i>.';
    }

    public function Msg_TicketNotFind() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, o ticket ( <b>@' . $this->getID_Ticket() . '</b> ) não encontra-se vinculado à sua conta, verifique se o mesmo não encontra-se incorreto!';
    }

    public function Msg_CreateFollowUp() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, seu acompanhamento foi encaminhado para central de atendimento!';
    }

    public function Msg_ErrorCreateFollowUp() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, infelizmente não foi possível responder seu chamado, tente novamente, ou entre em contato com o suporte técnico!';
    }

    public function Msg_FollowUpAttendance() {
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, segue o relatório do Ticket ( <b>@' . $this->getID_Ticket() . '</b> )!

<b>Descrição:</b>
' . $this->getMessage() . '
';
    }
    
    public function Msg_FollowUpAddedPlantao($bool){
        return 'Nova atualização do chamado ( <b>@'.$this->getID_Ticket().' feito por ' . $this->getFirstName() . ' ' . $this->getLastName() . '</b> )

<b>Descrição:</b>
'.$this->getMessage().'

'.($bool ? '<b>Contém anexos:</b> Sim':'');
    }

    public function Msg_StartBot(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, como é seu primeiro atendimento, você tem que realizar o cadastro do suporte. Para isso:

1- Selecione à bolinha (SM) ao lado desta mensagem;
2- Na nova conversa com o  Suporte Madero, selecione o ícone para enviar mensagem;
3- Digite à mensagem: <b>/start</b>.';
    }
    
    public function Msg_Started(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, agora podemos nos comunicar privadamente!';
    }
    
    public function Msg_ErrorIdentificationFilial($name){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, não foi possível reconhecer à identificação da filial para abrir um chamado, tente novamente e siga sempre o padrão:

Para abrir um chamado em nosso suporte cite o meu nome
<b>' . $name . '</b> número_da_filial <i>texto</i>.

<b>Exemplo:</b>
'.$name.' 123 Problemas técnicos.';
    }
    
    public function Msg_CreateChatGroup(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, você deve abrir o chamado no grupo de Atendimento Madero!';
    }
    
    public function Msg_NullChr64(){
        return 'Olá <b>' . $this->getFirstName() . ' ' . $this->getLastName() . '</b>, você deve responder o chamado com <b>@numero_de_Ticket</b> <i>texto</i>.';
    }
}

?>