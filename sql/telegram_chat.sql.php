<?php
//Types_Search
define('FromID', 1);
define('UserID', 2);

class mySQL {
    function Connect() {
        return mysqli_connect("localhost", "root", "root", "glpi");
    }
}

class account {
    function account_exists($Telegram, $Type) {
        $query = "SELECT * FROM glpi_users WHERE ";

        if ($Type == FromID)
            $query = $query . "Telegram_FromID = " . $Telegram->getFrom_ID();

        if ($Type == UserID)
            $query = $query . "name = " . $Telegram;

        $MySQL = new mySQL();
        $conn = $MySQL->Connect();

        $exec = mysqli_query($conn, $query);

        mysqli_close($conn);

        if (mysqli_num_rows($exec) > 0)
            return true;                              //Cadastro existente
        else
            return false;                              //Cadastro inexistente
    }
    
    function UpdateMyID_Telegram($Telegram){
        $MySQL  = new mySQL();
        $conn   = $MySQL->Connect();
        $query  = "SELECT id FROM glpi_users WHERE name = '".trim($Telegram->getMessage())."'";
        $exec   = mysqli_query($conn, $query);
        $row    = mysqli_num_rows($exec);
        
        if($row){
            $query = "UPDATE glpi_users SET Telegram_FromID = ".$Telegram->getFrom_ID()." WHERE name = '".trim($Telegram->getMessage())."'";
            $exec  = mysqli_query($conn, $query);
            
            mysqli_close($conn);
            
            if($exec){ return true; }
            
        }
        return false;
    }

    function getID($Telegram, $Type) {
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();

        $query = "SELECT id FROM glpi_users WHERE ";

        if ($Type == FromID)
            $query = $query . "Telegram_FromID = " . $Telegram->getFrom_ID();

        if ($Type == UserID)
            $query = $query . "name = '" . $Telegram->getUser() . "'";

        $exec = mysqli_query($conn, $query);

        mysqli_close($conn);

        if ($exec) {
            $dados = mysqli_fetch_row($exec);
            return $dados[0];
        } else
            return false;
    }
    
    function limitarTexto($texto, $limite){
        $texto = str_replace(Boot_Name, '', $texto);
        $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
        return $texto;
    }

    function CreateTicket($Telegram) {
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();
        $ID_User = $this->getID($Telegram, FromID);

        $Data_H = date('Y-m-d H:i:s');

        $query = "INSERT INTO glpi_tickets(name,
                                           date,
                                           date_mod, 
                                           users_id_lastupdater, 
                                           status, 
                                           users_id_recipient, 
                                           requesttypes_id, 
                                           content, 
                                           urgency, 
                                           impact, 
                                           priority, 
                                           type, 
                                           global_validation, 
                                           date_creation,
                                           locations_id)
                                    VALUES('[Tel]" . $this->limitarTexto($Telegram->getMessage(), 25) . "',
                                           '" . $Data_H . "',
			                   '" . $Data_H . "',
			                   '" . $ID_User . "',
			                   1,
			                   '" . $ID_User . "',
			                   7,
			                   '" . $Telegram->getMessage() . "',
			                   3,
			                   3,
			                   3,
			                   2,
			                   1,
			                   '" . $Data_H . "',
                                           ".$Telegram->getID_Filial().")";

        $exec = mysqli_query($conn, $query);

        if (!$exec) {
            return false;
        }

        $Telegram->setID_Ticket(mysqli_insert_id($conn));

        $query = "INSERT INTO glpi_tickets_users(tickets_id, users_id, type, use_notification)
												VALUES(" . $Telegram->getID_Ticket() . ", " . $ID_User . ", 1, 1)";

        $exec = mysqli_query($conn, $query);

        if (!$exec) {
            return false;
        }

        mysqli_close($conn);

        return true;
    }

    function Valid_CreateFollowUp($Telegram) {
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();

        $query = "SELECT users_id_recipient FROM glpi_tickets WHERE id = " . $Telegram->getID_Ticket();

        $exec = mysqli_query($conn, $query);
        $rows = mysqli_num_rows($exec);

        mysqli_close($conn);

        if ($rows <= 0) {
            return false;
        }
        return true;
    }

    function CreateFollowUp($Telegram) {
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();

        $Data_H = date('Y-m-d H:i:s');

        $query = "INSERT INTO glpi_ticketfollowups(tickets_id, date, users_id, content, requesttypes_id, date_mod)
                                VALUES (" . $Telegram->getID_Ticket() . ",
				'" . $Data_H . "',
				" . $this->getID($Telegram, FromID) . ",
				'" . $Telegram->getMessage() . "',
				1,
				'" . $Data_H . "')";
        $exec = mysqli_query($conn, $query);

        mysqli_close($conn);

        if (!$exec) {
            return false;
        }

        return true;
    }

    function CreateDocumment($Telegram, $PhotoDownload) {
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();

        $Data_H = date('Y-m-d H:i:s');

        $query = "INSERT INTO glpi_documents(name,
			                    filename,
			                    filepath,
			                    mime,
			                    date_mod,
			                    users_id,
			                    tickets_id,
			                    sha1sum,
			                    date_creation)
				VALUES('Documento do chamado " . $Telegram->getID_Ticket() . "',
					'" . $PhotoDownload->getArqName() . "',
					'JPG/" . $PhotoDownload->getHashFile()[0] . $PhotoDownload->getHashFile()[1] . chr(47) . $PhotoDownload->getFileName() . "',
					'image/jpeg',
					'" . $Data_H . "',
					" . $this->getID($Telegram, FromID) . ",
					" . $Telegram->getID_Ticket() . ",
					'" . $PhotoDownload->getHashFile() . "',
					'" . $Data_H . "')";

        $exec = mysqli_query($conn, $query);

        $id_doc = mysqli_insert_id($conn);

        $query = "INSERT INTO glpi_documents_items(documents_id,
			                            items_id,
			                            itemtype,
			                            date_mod)
					VALUES( " . $id_doc . ",
			      		 	" . $Telegram->getID_Ticket() . ",
			      		   	'Ticket',
			      		 	'" . $Data_H . "')";

        $exec = mysqli_query($conn, $query);

        mysqli_close($conn);

        if (!$exec) {
            return false;
        }

        return true;
    }

    function getDadosCreatorTicket($Telegram) {
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();

        $query = "SELECT glpi_users.id,
			 glpi_users.Telegram_FromID,
                         glpi_users.firstname,
			 glpi_users.realname
                  FROM	glpi_tickets, glpi_users
		  WHERE	glpi_tickets.users_id_recipient = glpi_users.id AND glpi_tickets.id = " . $Telegram->getID_Ticket();

        $exec = mysqli_query($conn, $query);

        $dados = mysqli_fetch_row($exec);

        if (!trim($dados[1]))
            return false;

        mysqli_close($conn);

        $Telegram->setFrom_ID($dados[1]);
        //setChat_ID utilizado apenas para não ter q criar outro set para o ID do User
        $Telegram->setChat_ID(intval($dados[0]));
        $Telegram->setFirstName($dados[2]);
        $Telegram->setLastName($dados[3]);

        return true;
    }

    function getDadosUser($Telegram){
    	$MySQL = new mySQL();
	$conn = $MySQL->Connect();
	
	$query = "SELECT firstname, realname FROM glpi_users WHERE id = ".$Telegram->getChat_ID();
	
	$exec = mysqli_query($conn, $query);
	
	$dados = mysqli_fetch_row($exec);

	mysqli_close($conn);
	
	return $dados;
    }
    
    function getTelegram_FromID($Telegram){
        $MySQL = new mySQL();
        $conn = $MySQL->Connect();
        
        $query = "SELECT firstname, realname, Telegram_FromID FROM glpi_users WHERE id = ".$Telegram->getChat_ID();
        
        $exec  = mysqli_query($conn, $query);
        
        $dados = mysqli_fetch_row($exec);
        
        if(!trim($dados[2])) return false;
        
        mysqli_close($conn);
        
        return $dados;
    }
    
    function Valid_LocationID($Telegram){
        $MySQL  = new mySQL();
        $query  = "SELECT id FROM glpi_locations WHERE id = ".$Telegram->getID_Filial();
        $conn   = $MySQL->Connect();
        
        $exec   = mysqli_query($conn, $query);
        $row    = mysqli_num_rows($exec);
        
        mysqli_close($conn);
        
        if($row <= 0) return false;
        
        return true;
    }
}
?>
