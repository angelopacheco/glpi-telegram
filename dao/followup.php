<?php
  define('Plantao_ID' , '-223836578');                                          //ID do Chat de plantão
  include ('../Telegram/class/telegram_chat.class.php');
  include ('../Telegram/sql/telegram_chat.sql.php');
  include ('../Telegram/dao/telegram_chat.php');
  include ('../Telegram/inc/telegram_api.inc.php');
  
  function Telegram_SendFollowUp($post, $session){
      if(isset($post) && isset($session))
      {
        $Telegram   = new TelegramChat();
        $Account    = new Account();

        $Telegram->setID_Ticket(intval($post[tickets_id]));
        $Telegram->setMessage($post['content']);
        $Telegram->setStatus($post['_status']);

        if(!trim($Telegram->getMessage())) return false;

        if(isset($post['_filename'])){
          $text = "

<b>Documentos anexados</b>:";
          
          if(count($post['_filename']) > 0){
            for($i = 0; $i < count($post['_filename']); $i++)
              $text .= "
- ".$post['_filename'][$i]."";
          }
          $Telegram->setMessage($Telegram->getMessage().$text);
        }
        
        $text = "

<b>Status do chamado: </b>".$Telegram->getStatus()."

<b>Link do chamado:</b>
https://servicedesk.restaurantemadero.com.br/front/ticket.form.php?id=".$Telegram->getID_Ticket();
        $Telegram->setMessage($Telegram->getMessage().$text);
        

	function sendPlantao($Telegram, $session){
	    $Telegram->setFirstName($session['glpifirstname']);
	    $Telegram->setLastName($session['glpirealname']);
	    apiRequest("sendMessage", array("parse_mode" => 'HTML',
					    "chat_id" => Plantao_ID,
					    "text" => $Telegram->Msg_FollowUpAddedPlantao(isset($post['_filename']))));
	}

        if($Account->getDadosCreatorTicket($Telegram))
        {
          if($Telegram->getChat_ID() != intval($session['glpiID']))             //Verifica se não é o criador do Ticket respondendo o chamado
          {
            apiRequest("sendMessage", array("parse_mode" => 'HTML',
                                            "chat_id" => $Telegram->getFrom_ID(),       //envia o texto
                                            "text" => $Telegram->Msg_FollowUpAttendance()));
          } else {
	    sendPlantao($Telegram, $session);
          }
        }else{
	    sendPlantao($Telegram, $session);
	}
      }
  }
  ?>
