<?php
define('BOT_TOKEN'  , '435674361:AAEaXsJerZNHf_RrgrcMipDNxiYaP8bN1GQ');         //Token gerado pelo Telegram do BOT
define('API_URL'    , 'https://api.telegram.org/bot' . BOT_TOKEN . '/');        //Url do Bot com o Token
define('GetFile_URL', API_URL . 'getFile?file_id=');                            //Url Get File, pega o Path dos arquivos para download
define('Boot_Name'  , '@maderoti_bot');                                         //Nome do BOT
define('Chat_ID'    , '-224578667');                                            //ID do Chat que abre chamado
define('Plantao_ID' , '-223836578');                                            //ID do Chat de plantão

function TestConn() {
    $MySQL = new mySQL();
    $conn = $MySQL->Connect();
    mysqli_close($conn);
    return $conn;
}

function CreateDocumment($Telegram, $Account) {
    $PhotoDownload = DownloadPhoto($Telegram);                                  //Realiza o download da foto e retorna a classe DownloadPhoto

    if (!$PhotoDownload->getBool())
        return false;

    if (!$Account->CreateDocumment($Telegram, $PhotoDownload))
        return sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_ErrorCreateDocumment());
}

function sendMsgPlantao($Telegram, $bool) {
    $hora = date('H', $Telegram->getDate());
    $min  = date('i', $Telegram->getDate());
    if (($hora >= 8 && $hora <= 23) || ($hora == 0 && $min <= 30)) {
        sendMsg(Plantao_ID, $Telegram->Msg_DadosTicketPlantao($bool));
    }
}

function CreateTicket($Telegram, $Account, $Type) {
    //Procedimento para criar ticket
    if ($Account->CreateTicket($Telegram)) {
        $bool = false;
        sendMsg($Telegram->getChat_ID(), $Telegram->Msg_CreateTicket());
        sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_DadosTicket());

        if ($Type == Photo) {
            CreateDocumment($Telegram, $Account);
            $bool = true;
        }
        
        sendMsgPlantao($Telegram, $bool);
    } else
        sendMsg($Telegram->getChat_ID(), $Telegram->Msg_FailCreateTicket());
}

function CreateFollowUp($Telegram, $Account, $bool_msg) {
    if (!$Account->CreateFollowUp($Telegram))
        return sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_ErrorCreateFollowUp());

    sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_CreateFollowUp());
    sendMsg(Plantao_ID, $Telegram->Msg_FollowUpAddedPlantao($bool_msg));
}

function recv($message, $type) {
    $Telegram = new TelegramChat();
    $Account = new Account();

    if ($type == Msg) {
        $Telegram->setChat_ID($message['chat']['id']);
        $Telegram->setMessage($message['text']);
        $Telegram->setDate($message['date']);
        $Telegram->setFrom_ID($message['from']['id']);
        $Telegram->setFirstName($message['from']['first_name']);
        $Telegram->setLastName($message['from']['last_name']);
    }

    if ($type == Photo) {
        $Telegram->setChat_ID($message['chat']['id']);
        $Telegram->setMessage($message['caption']);
        $Telegram->setFrom_ID($message['from']['id']);
        $Telegram->setFirstName($message['from']['first_name']);
        $Telegram->setLastName($message['from']['last_name']);
        $pos = count($message['photo'])-1;
        $Telegram->setFile_ID($message['photo'][$pos]['file_id']);              //**Realizar verificação se existe [2]

        $Photo = json_decode(file_get_contents(GetFile_URL . $Telegram->getFile_ID()), true); //Pega os dados de download da foto
        $Telegram->setFile_Path($Photo['result']['file_path']);
    }

    if (!TestConn()) {                                                          //Testa à conexão com o MySQL
        sendMsg($Telegram->getChat_ID(), $Telegram->Msg_ErrorMySQL());
        exit;
    }

    if (($Telegram->getMessage() == '/start') && (isMemberGroup($Telegram->getFrom_ID()))) {
        if (!$Account->account_exists($Telegram, FromID)) {                     //Verifica se o ID do Telegram está vinculado em alguma conta
            sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_AttachAccount_());
        } else
            sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_Started());
        exit;
    }

    //Trabalha com as mensagens do grupo de chamado
    if ($Telegram->getChat_ID() == Chat_ID) {                                   //Verifica se é o grupo de chamados
        $Dados_ValidaMsg = sendMsg($Telegram->getFrom_ID(), 'Validando conta...');

        if (!$Dados_ValidaMsg['ok']) {
            sendMsg($Telegram->getChat_ID(), $Telegram->Msg_StartBot());
            exit;
        } else
            deleteMsg($Dados_ValidaMsg['result']['chat']['id'], $Dados_ValidaMsg['result']['message_id']);
        
        if ((strripos($Telegram->getMessage(), Boot_Name) === false)) {         //Verifica se foi digitado o @user na Msg.
            sendMsg($Telegram->getChat_ID(), $Telegram->Msg_Flood(Boot_Name));
            exit;
        }

        if (!$Account->account_exists($Telegram, FromID)) {                     //Verifica se o ID do Telegram está vinculado em alguma conta
            sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_AttachAccount());
            exit;
        }

        if (!$Telegram->ExtractID_Filial()) {                                   //Extrai o ID da filial da mensagem enviada
            sendMsg($Telegram->getChat_ID(), $Telegram->Msg_ErrorIdentificationFilial(Boot_Name));
            exit;
        }

        if (!$Account->Valid_LocationID($Telegram)) {                           //Verifica se existe no banco de dados o ID
            sendMsg($Telegram->getChat_ID(), $Telegram->Msg_ErrorIdentificationFilial(Boot_Name));
            exit;
        }

        if ($type == Msg)
            CreateTicket($Telegram, $Account, Msg);

        if ($type == Photo)
            CreateTicket($Telegram, $Account, Photo);
        exit;
    }

    if (!isMemberGroup($Telegram->getFrom_ID())) {
        exit;
    }
    
    //Demais chat    
    if ($Telegram->getMessage()[0] == chr(64)) {                                //Verifica se tem um @ na Mensagem
        if (!$Account->account_exists($Telegram, FromID)) {                     //Verifica se o usuário tem conta vinculada
            $user = explode(chr(64), $Telegram->getMessage());                  //Tira o @ da mensagem
            $Telegram->setMessage($user[1]);                                    //Seta o nome sem o @

            if ($Account->UpdateMyID_Telegram($Telegram))
                sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_AttachedSuccess(Boot_Name));
            else
                sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_AttachedError());
            exit;
        }

        $explode = explode(chr(32), $Telegram->getMessage());                   //Separa a mensagem por espaço em um array
        $intexplode = preg_replace("/[^0-9]/", "", $explode[0]);                //Separa numeros de caracteres

        $Telegram->setID_Ticket(intval($intexplode));

        if ($Telegram->getID_Ticket() < 1) {
            if(!(strripos($Telegram->getMessage(), Boot_Name) === false))
                sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_CreateChatGroup());
            exit;
        }

        if (!$Account->Valid_CreateFollowUp($Telegram))                         //Verifica se o ID_Ticket pertence à conta
            return sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_TicketNotFind());

        if ($type == Photo)
            CreateDocumment($Telegram, $Account);                               //Adiciona o acompanhamento com o anexo

        if (strlen($Telegram->getMessage()) > (strlen(strval($Telegram->getID_Ticket())) + 1)) {
            //$explode = str_replace($explode[0], "", $Telegram->getMessage());   //Separa o user da mensagem do ticket
            //$Telegram->setMessage($explode);
            CreateFollowUp($Telegram, $Account, ($type == Photo));
        } else{
            sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_CreateFollowUp());
            sendMsg(Plantao_ID, $Telegram->Msg_FollowUpAddedPlantao($type == Photo));
        }
        
        exit;
    }else
        sendMsg($Telegram->getFrom_ID(), $Telegram->Msg_NullChr64());
}

?>